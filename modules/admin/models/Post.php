<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "{{%post}}".
 *
 * @property int $id
 * @property string $subject عنوان
 * @property string $text متن
 * @property int $created_at ساخته شده در
 * @property int $updated_at ویرایش شده در
 *
 * @property Comment[] $comments
 * @property Posttag[] $posttags
 * @property Tag[] $tags
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'text'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'عنوان',
            'text' => 'متن',
            'created_at' => 'ساخته شده در',
            'updated_at' => 'ویرایش شده در',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosttags()
    {
        return $this->hasMany(Posttag::className(), ['post_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('{{%posttag}}', ['post_id' => 'id']);
    }
    
    public function getTagList(){
        $tag= Tag::find()->all();
        return ArrayHelper::map($tag, 'id', 'name');
    }
    
    public function getSelectedTag(){
        $selected=[];
        if($this->isNewRecord !=1){
            $selected= Posttag::find()->where(['post_id'=> $this->id])->all();
            return ArrayHelper::getColumn($selected, 'tag_id');
        }
        return $selected;
    }
    
    public function afterSave($insert, $changedAttributes) {
        if(isset($_POST['posttags'])){
            $selected= Yii::$app->request->post('posttags');
            Posttag::deleteAll(['post_id'=> $this->id]);
            $insertData=[];
            foreach ($selected as $tagId){
                $insertData[]=[$this->id,$tagId];
            }
            Yii::$app->db->createCommand()->batchInsert('Posttag', ['post_id' , 'tag_id'], $insertData)->execute();
        }
    }
}
