<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Default controller for the `admin` module
 */
class MyController extends Controller {

    public function init() {
        if(Yii::$app->user->isGuest)   $this->goHome ();
        $role= Yii::$app->authManager->getAssignment('admin', Yii::$app->user->id);
        $role=$role != NULL ? $role->roleName : '';
        if($role!='admin')     $this->goHome ();
        
        $this->layout = 'main';
        parent::init();
    }

}
