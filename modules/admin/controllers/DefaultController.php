<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\AccessControl;
/**
 * Default controller for the `admin` module
 */
class DefaultController extends MyController
{
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        
        Yii::$app->user->returnUrl= Url::to();
        return $this->render('index');
    }
    public function actionInit(){
        $auth= Yii::$app->authManager;
        $admin=$auth->createRole('admin');
        $auth->add($admin);
        $auth->assign($admin, 1);
    }
}
