<?php
namespace app\models;

 use Yii;
 use yii\base\Model;

 class FirstForm extends Model{
     public $username;
     public $password;
     public $email;
     
     
     public function attributeLabels() {
         return [
           'username' => 'نام کاربری',
             'password'=>'رمز عبور',
             'email' => 'ایمیل',
         ];
     }
     
     public function rules() {
         return [
           [['username' , 'password'] , 'required'],
             ['email' , 'email'],
         ];
     }
 }