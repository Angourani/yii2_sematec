<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property int $id
 * @property string $from نام و نام خانوادگی
 * @property string $email ایمیل
 * @property string $text متن
 * @property int $post_id
 * @property int $created_at ساخته شده در
 * @property int $updated_at ویرایش شده در
 *
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'email', 'text', 'post_id'], 'required'],
            [['text'], 'string'],
            [['post_id', 'created_at', 'updated_at'], 'integer'],
            [['from', 'email'], 'string', 'max' => 255],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'نام و نام خانوادگی',
            'email' => 'ایمیل',
            'text' => 'متن',
            'post_id' => 'Post ID',
            'created_at' => 'ساخته شده در',
            'updated_at' => 'ویرایش شده در',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
}
