<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\FirstForm;
use app\models\Post;
use app\models\Comment;
use app\models\SignupForm;
use PicoFeed\Syndication\Rss20FeedBuilder;
use PicoFeed\Syndication\Rss20ItemBuilder;
use yii\helpers\Url;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'signup'],
                'rules' => [
                        [
                        'allow' => TRUE,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                        [
                        'allow' => TRUE,
                        'actions' => ['logout'],
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

        $query = Post::find()->orderBy('created_at DESC');
        $pagination = new \yii\data\Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $title = Post::find()->select('subject')->orderBy('id DESC')->all();

        $model = $query->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();

        $tag= \app\models\Tag::find()->all();
        return $this->render('index', ['posts' => $model,'tags'=>$tag, 'titles' => $title, 'pagination' => $pagination]);
    }

    public function actionPost() {
        $comments = Comment::find()->all();
        $comment = new Comment();
        $titles = Post::find()->select('subject')->all();
        $title = \yii\helpers\Html::encode(Yii::$app->request->get('title'));
        $post = Post::find()->where(['subject' => $title])->one();

        $comment->post_id = $post->id;
        if ($comment->load(Yii::$app->request->post()) && $comment->validate()) {
            $comment->created_at = time();
            $comment->save();
            Yii::$app->session->setFlash('PostCreated');
            return $this->redirect(['post', 'title' => $post->subject]);
        }
        return $this->render('post', ['model' => $post, 'comment' => $comment, 'comments' => $comments, 'titles' => $titles]);
    }
    
    public function actionTag(){
        $tag= \yii\helpers\Html::encode(Yii::$app->request->get('title'));
        $tag= \app\models\Tag::find()->where(['name'=>$tag])->one();
        $posts=$tag->getPosts()->all();
        $title = Post::find()->select('subject')->orderBy('id DESC')->all();
        $tags= \app\models\Tag::find()->all();
        return $this->render('tag',['model'=>$posts,'tags'=>$tags, 'titles' => $title]);
    }

    public function actionShow() {
        $model = new FirstForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->render('showpost', ['model' => $model]);
        } else {
            return $this->render('develop', ['model' => $model]);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionRss() {
        //echo Url::to(['site/rss'],TRUE);exit;
        $feedBuilder = Rss20FeedBuilder::create()
                ->withTitle('وبسایت خبری من')
                ->withAuthor('admin')
                ->withFeedUrl(Url::to(['site/rss']))
                ->withSiteUrl(Url::to(['site/index'],TRUE))
                ->withDate(new \DateTime());
        $posts = Post::find()->orderBy('id DESC')->limit(5)->all();
        foreach ($posts as $post) {
            $text= mb_substr($post->text, 0,150).'..........';
            $feedBuilder
                    ->withItem(Rss20ItemBuilder::create($feedBuilder)
                            ->withTitle($post->subject)
                            ->withUrl(Url::to(['site/post','title'=>$post->subject],TRUE))
                            ->withAuthor('admin')
                            ->withPublishedDate(new \DateTime('@'.$post->created_at))
                            ->withSummary($text)
                            ->withContent($text)
            );
        }
        echo $feedBuilder->build();
    }

}
