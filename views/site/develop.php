<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="col-md-6">
    
    <?php $form= ActiveForm::begin(); ?>

    <?= $form->field($model, 'username'); ?>

    <?= $form->field($model, 'password')->passwordInput(); ?>

    <?= $form->field($model, 'email'); ?>


    <?= Html::submitButton('Send ' , ['class' => 'btn btn-primary']); ?>
    <?php $form= ActiveForm::end(); ?>

</div>