<?php
if (empty($model))
    exit;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
$this->title = 'صفحه اصلی';
?>



<div class="row">
    <div class="col-md-2">
        <div class="panel panel-default mrg-btm-10">
            <div class="panel-heading">آخرین مطالب</div>
            <div class="panel-body">
                <?php
                foreach ($titles as $title) :
                    ?>
                    <a href="<?= Url::to(['site/post', 'title' => $title->subject]) ?>"><?= $title->subject ?></a>

                    <hr class="no-margin padd-top-btm-5">
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-md-10">


        
        <div class="panel panel-default mrg-btm-10">
            <div class="panel-heading text-center"><?= $model->subject ?></div>
            <div class="panel-body">
                <?= $model->text ?>
            </div>
        </div>



        <div class="col-md-12 thumbnail padd-14">
            <h3 class="page-header text-success">نظرات شما</h3>
            <?php foreach ($comments as $c) : ?>
                <div class="alert bg-info border-pnlcomment"> نوشته شده توسط :<?= $c->from ?> در تاریخ :<?= Yii::$app->formatter->asDate($c->created_at, 'php:d-m-Y') ?>
                    <hr class="hrComment">
                    <p class="text-justify"><?= $c->text ?></p>
                </div>
            <?php endforeach; ?>

        </div>
        <div class="clearfix"></div>

        <?php if (!Yii::$app->session->hasFlash('PostCreated')) : ?>
            <div class="col-md-12 thumbnail padd-14">
                <br>
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($comment, 'from')->textInput(['maxlength' => true]) ?>

                <?= $form->field($comment, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($comment, 'text')->textarea(['rows' => 6]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        <?php else : ?>
            <div class="alert alert-success">پیغام شما با موفقیت ثبت شد</div>
        <?php endif; ?>

    </div>
</div>

