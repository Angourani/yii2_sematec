<?php

use yii\helpers\Url;
?>
<div class="row">

    <div class="col-md-2">
        <div class="panel panel-default mrg-btm-10">
            <div class="panel-heading">آخرین مطالب</div>
            <div class="panel-body">
                <?php
                foreach ($titles as $title) :
                    ?>
                    <a href="<?= Url::to(['site/post', 'title' => $title->subject]) ?>"><?= $title->subject ?></a>

                    <hr class="no-margin padd-top-btm-5">
                <?php endforeach; ?>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">موضوعات</h3>
            </div>
            <div class="panel-body">
                <ul>
                <?php foreach ($tags as $tag): ?>
                    <li><a href="<?= Url::to(['site/tag','title'=>$tag->name])?>"><?=$tag->name?></a></li>
                <?php     endforeach; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-10">
        <?php foreach ($model as $post) : ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= $post->subject ?></h3>
                </div>
                <div class="panel-body">
                    <?= $post->text ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>